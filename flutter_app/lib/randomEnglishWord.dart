import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class RandomEnglishWord extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new RandomEnglishWordsState();
  }
}

class RandomEnglishWordsState extends State<RandomEnglishWord> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final wordPair = WordPair.random();
    return Text(
      wordPair.asUpperCase,
      style: TextStyle(fontSize: 20.0),
    );
  }
}
