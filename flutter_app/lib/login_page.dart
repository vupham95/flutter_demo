import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/KeyboardUtils.dart';
import 'package:flutter_app/home_page.dart';
import 'package:flutter_app/login_model.dart';
import 'package:validate/validate.dart';

class LoginPage extends StatefulWidget {
  static String tag = "login-page";
  @override
  State<StatefulWidget> createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;
  bool _isChecked = false;
  final String _notifyLoginFailed =
      "Email or password is incorrect.Please check again !";
  final String _notifyLoginSuccess =
      "Login success.Wellcome to NextFunc company !";
  final String _confirmEmail = "vu.pham@nextfunc.com";
  final String _confirmPassword = "12345678";
  final int timeShowSnackBar = 3000;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Timer _timer;
  LoginData _data = new LoginData();
  KeyBoardUtils _keyBoardUtils = new KeyBoardUtils();

  String _validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }

    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }

    return null;
  }

  void validateForm() {
    // hide keyboard
    _keyBoardUtils.hideKeyBoard(context);
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      if (_data.email == _confirmEmail && _data.password == _confirmPassword) {
        showStatusLogin(_notifyLoginSuccess, true);
        delayNavigationToHomePage();
        print("login success");
      } else
        showStatusLogin(_notifyLoginFailed, false);
    }
  }

  void showStatusLogin(String value, bool loginSuccess) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(value),
        backgroundColor: loginSuccess ? Colors.blue : Colors.red,
        duration: Duration(milliseconds: timeShowSnackBar)));
  }

  void delayNavigationToHomePage(){
    new Timer(new Duration(milliseconds: timeShowSnackBar), () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    });
  }
  @override
  void initState() {
    super.initState();
    _iconAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000));
    _iconAnimation = CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.bounceOut);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  void onChanged(bool value) {
    setState(() {
      _isChecked = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: new BoxDecoration(
          color: Color(0xFFeeece9),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 80.0),
          child: ListView(
            children: <Widget>[
              FlutterLogo(
                size: _iconAnimation.value * 100,
              ),
              Form(
                key: this._formKey,
                child: Theme(
                  data: ThemeData(
                      brightness: Brightness.light,
                      primarySwatch: Colors.blue,
                      inputDecorationTheme: InputDecorationTheme(
                          labelStyle:
                          TextStyle(color: Colors.blue, fontSize: 16.0))),
                  child: Container(
                    padding: const EdgeInsets.all(40.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        TextFormField(
                            style: TextStyle(
                                color: Color(0xEE29b6f6),
                                fontSize: 13.0,
                                fontStyle: FontStyle.normal),
                            autofocus: false,

                            decoration: InputDecoration(
                              hintText: "vu.pham@nextfunc.com",
                              labelText: "Enter email",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)
                              )
                            ),
                            keyboardType: TextInputType.emailAddress,
                            validator: this._validateEmail,
                            onSaved: (String value) {
                              this._data.email = value;
                            }),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: TextFormField(
                              style: TextStyle(
                                  color: Color(0xEE29b6f6),
                                  fontSize: 13.0,
                                  fontStyle: FontStyle.normal),
                              decoration: InputDecoration(
                                hintText: "********",
                                labelText: "Enter password",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0)
                                  ),
                              ),
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              validator: this._validatePassword,
                              onSaved: (String value) {
                                this._data.password = value;
                              }),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: _isChecked,
                                    onChanged: (bool value) {
                                      onChanged(value);
                                    },
                                  ),
                                  Text(
                                    "Auto log-in",
                                    style: TextStyle(color: Color(0xEE29b6f6)),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Container(
                                  width: 300.0,
                                  child: RaisedButton(
                                    color: Color(0xEE29b6f6),
                                    splashColor: Color(0xEE81D4FA),
                                    onPressed: this.validateForm,
                                    child: Text(
                                      'SIGN-IN',
                                      style: TextStyle(
                                          fontSize: 15.0,
                                          fontStyle: FontStyle.normal,
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, top: 20.0),
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Sign up for an account",
                                style: TextStyle(
                                    color: Color(0xEE29b6f6),
                                    fontSize: 13.0,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.w300),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Text(
                                  "|",
                                  style: TextStyle(
                                    color: Color(0xEE29b6f6),
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                              Text(
                                "Forgot password",
                                style: TextStyle(
                                    color: Color(0xEE29b6f6),
                                    fontSize: 13.0,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
