import 'package:flutter/material.dart';
import 'package:flutter_app/home_page.dart';
import 'package:flutter_app/login_page.dart';
import 'package:flutter_app/randomEnglishWord.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  final routers = <String ,WidgetBuilder>{
    LoginPage.tag:(context) =>LoginPage(),
    HomePage.tag : (context) => HomePage(),
  };
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new LoginPage(),
      routes: routers,
    );
  }
}
