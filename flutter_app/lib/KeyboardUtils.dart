import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';

class KeyBoardUtils {
  void hideKeyBoard(BuildContext context){
    FocusScope.of(context).requestFocus(new FocusNode());
  }
}